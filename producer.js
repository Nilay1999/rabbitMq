const ampq = require('amqplib');
const message = {number : process.argv[2]}

connect();

async function connect(){
    try {
        const connection = await ampq.connect("amqp://localhost:5672");
        const channel = await connection.createChannel();

        const result = channel.assertQueue("jobs")
        channel.sendToQueue("jobs",Buffer.from(JSON.stringify(message)));
        console.log(`Result sent ${message.number}`)
        
    } catch (error) {
        console.error(error)
    }
} 